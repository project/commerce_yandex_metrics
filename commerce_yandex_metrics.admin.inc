<?php

/**
 * @file
 * Administrative callbacks for the Commerce Yandex.Metrics module.
 */

/**
 * Builds Commerce Yandex.Metrics settings form.
 */
function commerce_yandex_metrics_settings_form($form, &$form_state) {
  $form['commerce_yandex_metrics_goal_cart_view'] = array(
    '#type' => 'textfield',
    '#title' => t('Cart view goal'),
    '#default_value' => variable_get('commerce_yandex_metrics_goal_cart_view', 'CART_VIEW'),
    '#description' => t('The Yandex.Metrics goal name that fires during cart view page.'),
  );

  $form['commerce_yandex_metrics_goal_order_complete'] = array(
    '#type' => 'textfield',
    '#title' => t('Order complete goal'),
    '#default_value' => variable_get('commerce_yandex_metrics_goal_order_complete', 'ORDER_COMPLETE'),
    '#description' => t('The Yandex.Metrics goal name that fires during order complete page.'),
  );

  return system_settings_form($form);
}
