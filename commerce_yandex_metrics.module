<?php

/**
 * @file
 * Adds the required Javascript to the checkout view and completion pages
 * to allow e-commerce statistics tracking through Yandex Metrics.
 *
 * Refer to http://help.yandex.ru/metrika/?id=1115036
 * for documentation on the functions used to submit e-commerce statistics to
 * Yandex Metrics.
 */

/**
 * Implements hook_menu().
 */
function commerce_yandex_metrics_menu() {
  $items = array();

  $items['admin/config/system/yandex_metrics/commerce'] = array(
    'access arguments' => array('administer Yandex.Metrics settings'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_yandex_metrics_settings_form'),
    'file' => 'commerce_yandex_metrics.admin.inc',
    'title' => 'E-commerce settings',
    'type' => MENU_LOCAL_TASK,
    'weight' => 4
  );

  return $items;
}

/**
 * Implements hook_commerce_checkout_router().
 */
function commerce_yandex_metrics_commerce_checkout_router($order, $checkout_page) {
  // Add the javascript only when we are on the order checkout or complete page.
  if (in_array($checkout_page['page_id'], array('complete', 'checkout'))) {
    $metrics_goal_name = variable_get('commerce_yandex_metrics_goal_cart_view', 'CART_VIEW');
    if ($checkout_page['page_id'] == 'complete') {
      $metrics_goal_name = variable_get('commerce_yandex_metrics_goal_order_complete', 'ORDER_COMPLETE');
    }

    $script = commerce_yandex_metrics_ecommerce_js($order, $metrics_goal_name);

    // Add the code to the footer.
    drupal_add_js($script, array(
      'type' => 'inline',
      'scope' => 'footer',
      'preprocess' => FALSE,
    ));
  }
}


/**
 * Build the e-commerce JS passed to Yandex Metrics for order tracking.
 *
 * @param object $order
 *   The fully loaded order object to convert into yandex metrics goal JS.
 * @param string $metrics_goal_name
 *   The Yandex.mectrics goal name used on page load event.
 *
 * @return string
 *   The JS that should be added to the page footer.
 */
function commerce_yandex_metrics_ecommerce_js($order, $metrics_goal_name) {
  // Checking if Yandex.Metrics counter code placed on the site.
  $counter_code = variable_get('yandex_metrics_counter_code', '');

  if (empty($counter_code) || !preg_match("/yaCounter(\d+)/is", $counter_code, $matches) || !yandex_metrics_show_counter_for_role()) {
    return '';
  }

  $metrics_client_id = $matches[1];

  if (!($order instanceof EntityMetadataWrapper)) {
    $order = entity_metadata_wrapper('commerce_order', $order);
  }

  $transaction = array(
    'order_id' => $order->order_id->value(),
    'order_price' => commerce_currency_amount_to_decimal($order->commerce_order_total->amount->value(), $order->commerce_order_total->currency_code->value()),
    'currency' => $order->commerce_order_total->currency_code->value(),
    'exchange_rate' => 1,
    'goods' => array(),
  );

  // Allow modules to alter the transaction arguments.
  drupal_alter('commerce_yandex_metrics_transaction', $transaction, $order);

  $line_items = array();

  // Loop through the products on the order.
  foreach ($order->commerce_line_items as $line_item) {
    $properties = $line_item->getPropertyInfo();
    if (isset($properties['commerce_product'])) {
      // Build the item arguments.
      $item = array(
        'id' => $line_item->commerce_product->product_id->value(),
        'name' => $line_item->commerce_product->title->value(),
        'price' => commerce_currency_amount_to_decimal($line_item->commerce_unit_price->amount->value(), $line_item->commerce_unit_price->currency_code->value()),
        'quantity' => $line_item->quantity->value(),
      );
    }
    else {
      $item = array(
        'id' => $order->order_id->value(),
        'name' => $line_item->line_item_label->value(),
        'price' => commerce_currency_amount_to_decimal($line_item->commerce_unit_price->amount->value(), $line_item->commerce_unit_price->currency_code->value()),
        'quantity' => $line_item->quantity->value(),
      );
    }

    // Allow modules to alter the item arguments.
    drupal_alter('commerce_yandex_metrics_item', $item, $line_item, $transaction, $order);

    // Put the arguments into an array that is safe to implode directly.
    $line_items[] = $item;
  }

  $transaction['goods'] = $line_items;

  $script = '
  (function() {
    function f() {
      var yaParams = ' . drupal_json_encode($transaction) . ';
      yaCounter' . $metrics_client_id . '.reachGoal("' . $metrics_goal_name . '", yaParams);
    }

    if (window.addEventListener) {
        window.addEventListener("load", f, false);
    } else if (window.attachEvent) {
        window.attachEvent("onload", f);
    }
  })();
  ';
  return $script;
}
