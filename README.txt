Commerce Yandex.Mertics
=====================

This module provides e-commerce statistics tracking through Yandex.Metrics service.

Dependencies

   * Drupal Commerce (http://drupal.org/project/commerce);
   * Yandex.Metrics (http://drupal.org/project/yandex_metrics).

To install and configure

   1. Install and enable the module;
   2. Be sure that yandex metrics code successfully installed 
   (see Yandex.Metrics module installation Guide);
   3. Create new Yandex.Metrics goals, named "ORDER_COMPLETE" and "CART_VIEW" 
   at Yandex.Metrics admin interface;
   Refer to http://help.yandex.ru/metrika/?id=1115036 for more detail;
   4. ...
   5. Profit. Have a fun!


Commerce Yandex.Metrics module does 2 main things

   1. Create yandex metrics goal JS code;
   2. Add generated code to the DC order view and complete pages footer.
